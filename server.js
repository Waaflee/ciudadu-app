const {
  create,
  router: _router,
  defaults,
  bodyParser
  // eslint-disable-next-line @typescript-eslint/no-var-requires
} = require("json-server");
// server.js
const server = create();
const router = _router("db.json");
const middlewares = defaults();

server.use(middlewares);

server.use(bodyParser);

// If you need to scope this behaviour to a particular route, use this
server.post("/authentication", function(req, res, next) {
  req.method = "GET";
  req.query = req.body;
  next();
});

server.use(router);
server.listen(3030, function() {
  console.log("JSON Server is running");
});
