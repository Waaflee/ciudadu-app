module.exports = {
  transpileDependencies: ["feathers-vuex"],
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/assets/scss/_variables.scss";
          @import "@/assets/scss/_mixins.scss";
          @import "@/assets/scss/_juice.scss";
        `
      }
    }
  },
  configureWebpack: {
    devtool: "source-map"
  },
  devServer: {
    disableHostCheck: true
  }
};
