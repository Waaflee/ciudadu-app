import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueFormulate from "@braid/vue-formulate";
import "@mdi/font/css/materialdesignicons.min.css";
import Buefy from "buefy";
import VueCompositionApi from "@vue/composition-api";
import "@/assets/scss/_bulma.scss";
import "@/assets/scss/_fonts.scss";
import "@/assets/scss/_globals.scss";
import "@/assets/scss/_formulate.scss";
import VueFormulateOptions from "./VueFormulateOptions";
import VueAnimXYZ from "@animxyz/vue";
import "chartist/dist/scss/chartist.scss";
import "@animxyz/core"; // Import css here if you haven't elsewhere
import VueCarousel from "vue-carousel";
import GoBack from "@/components/GoBack.vue";
import FImage from "@/components/FImage.vue";
// import { VueReCaptcha } from "vue-recaptcha-v3";

Vue.config.productionTip = process.env.NODE_ENV === "production";

Vue.component("GoBack", GoBack);
Vue.component("f-img", FImage);
Vue.use(VueCarousel);
Vue.use(VueAnimXYZ);
Vue.use(VueFormulate, VueFormulateOptions);
Vue.use(Buefy);
Vue.use(VueCompositionApi);
// Vue.use(VueReCaptcha, { siteKey: "6Lep-YUaAAAAANc4s2W6ylwaiQOxngW_y7Cf2UV3" });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
