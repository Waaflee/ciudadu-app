// src/feathers-client.js
import feathers from "@feathersjs/feathers";
import auth from "@feathersjs/authentication-client";
import { iff, discard } from "feathers-hooks-common";
import feathersVuex from "feathers-vuex";
import axios from "axios";
import rest from "@feathersjs/rest-client";

const apiUrl = process.env.VUE_APP_API_URL || "http://localhost:3030";
const restClient = rest(apiUrl);
const transport = restClient.axios(axios);

const feathersClient = feathers()
  .configure(auth({ storage: window.localStorage }))
  .configure(transport)
  .hooks({
    before: {
      all: [
        iff(
          context => ["create", "update", "patch"].includes(context.method),
          discard("__id", "__isTemp")
        )
      ]
    }
  });

export default feathersClient;

// Setting up feathers-vuex
const {
  makeServicePlugin,
  makeAuthPlugin,
  BaseModel,
  models,
  FeathersVuex
} = feathersVuex(feathersClient, {
  serverAlias: "api", // optional for working with multiple APIs (this is the default value)
  idField: "_id", // Must match the id field in your database table/collection
  // whitelist: ["$regex", "$options"],
  enableEvents: false,
  debug: process.env.NODE_ENV === "production" ? false : true
});

export { makeAuthPlugin, makeServicePlugin, BaseModel, models, FeathersVuex };
