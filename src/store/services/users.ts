// src/store/services/users.js
import { ServiceState } from "feathers-vuex";
import { Id } from "feathers-vuex/dist/service-module/types";
import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class User extends BaseModel {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(data: any, options: any) {
    super(data, options);
  }
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "User";
  // Define default properties here
  static instanceDefaults() {
    return {
      email: "",
      password: ""
    };
  }
}
export interface Users {
  _id?: Id;
  email: string;
  password: string;
  rol: "user" | "business" | "business-admin" | "user-admin" | "admin";
  profilePicture?: string;
  businessPicture?: string;
  businessCategory?: string;
  businessName?: string;
  businessReview?: string;
  businessLink?: string;
  name?: string;
  lastName?: string;
  birthday?: string;
  dni?: number;
  university?: string;
  facultyAndCareer?: string;
  certificate?: string;
  registration?: number;
  sex?: string;
  phone?: string;
  address?: string;
  isConfirmed: boolean;
  googleId?: string;
  isVerified?: boolean;
  verifyToken?: string;
  verifyExpires?: Date;
  verifyChanges?: unknown;
  resetToken?: string;
  resetExpires?: Date;
}
const servicePath = "users";

declare module "feathers-vuex" {
  interface FeathersVuexStoreState {
    [servicePath]: ServiceState<User>;
  }

  // Only if you setup FeathersVuex without a serverAlias!!
  interface FeathersVuexGlobalModels {
    User: typeof User;
  }
}

const servicePlugin = makeServicePlugin({
  Model: User,
  service: feathersClient.service(servicePath),
  servicePath
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;
