// src/store/services/users.js
import { ServiceState } from "feathers-vuex";
import { Id } from "feathers-vuex/dist/service-module/types";
import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Code extends BaseModel {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(data: any, options: any) {
    super(data, options);
  }
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Code";
  // Define default properties here
  static instanceDefaults() {
    return {};
  }
}
export interface Codes {
  _id?: Id;
  code: string;
  used?: boolean;
  benefit: Id;
  user: Id;
}
const servicePath = "codes";

declare module "feathers-vuex" {
  interface FeathersVuexStoreState {
    [servicePath]: ServiceState<Code>;
  }

  // Only if you setup FeathersVuex without a serverAlias!!
  interface FeathersVuexGlobalModels {
    Code: typeof Code;
  }
}

const servicePlugin = makeServicePlugin({
  Model: Code,
  service: feathersClient.service(servicePath),
  servicePath
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;
