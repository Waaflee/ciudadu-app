// src/store/services/users.js
import { ServiceState } from "feathers-vuex";
import { Id } from "feathers-vuex/dist/service-module/types";
import feathersClient, {
  makeServicePlugin,
  BaseModel
} from "../../feathers-client";

class Benefit extends BaseModel {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(data: any, options: any) {
    super(data, options);
  }
  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = "Benefit";
  // Define default properties here
  static instanceDefaults() {
    return {};
  }
}
export interface Benefits {
  _id?: Id;
  business_id: unknown;
  name: string;
  type: string;
  picture?: string;
  category?: string;
  subcategory?: string;
  percentage?: number;
  acceptedPayments?: string[];
  validDays?: string[];
  uses: number;
  validated?: "yes" | "pending" | "denied";
}
const servicePath = "benefits";

declare module "feathers-vuex" {
  interface FeathersVuexStoreState {
    [servicePath]: ServiceState<Benefit>;
  }

  // Only if you setup FeathersVuex without a serverAlias!!
  interface FeathersVuexGlobalModels {
    Benefit: typeof Benefit;
  }
}

const servicePlugin = makeServicePlugin({
  Model: Benefit,
  service: feathersClient.service(servicePath),
  servicePath
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
});

export default servicePlugin;
