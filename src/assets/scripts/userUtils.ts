import store from "@/store";
import { models } from "feathers-vuex";
const { User } = models.api;
const authUser = store.getters["auth/user"];

const patch = (data: {}, params = {}) => {
  const { _id } = authUser;
  const user = new User({ ...authUser });
  user.patch();
};

export default {
  patch
};
