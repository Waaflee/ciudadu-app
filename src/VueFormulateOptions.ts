import axios from "axios";
import { Context } from "./vue-formulate.types";

const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_FILES_URL
});

const getInputClasses = (type: string): string[] => {
  let classes: string[] = [""];
  switch (type) {
    case "checkbox":
      break;
    case "radio":
      break;
    case "select":
      classes = ["select", "input"];

    // eslint-disable-next-line no-fallthrough
    case "date":
      classes = [...classes, "input-has-alt-padding", "is-fullwidth"];
      break;
    case "submit":
      classes = ["button", "is-info", "my-5", "is-fullwidth", "input"];
      break;
    case "button":
      classes = [
        "button",
        "is-info",
        "is-outlined",
        "my-5",
        "is-fullwidth",
        "input"
      ];
      break;
    case "file":
      // classes = ["file-input"];
      break;
    default:
      classes = ["is-fullwidth", "input"];
      break;
  }
  return classes;
};

export default {
  uploader: axiosInstance,
  uploadUrl: "/uploads",
  classes: {
    outer: ["field"],
    wrapper: ["control"],
    label: ["label", "has-text-grey"],
    element: [""],
    input: (context: Context, classes: string[]) => {
      return classes.concat([...getInputClasses(context.type)]);
    },
    help: ["help"],
    errors: ["help is-danger"],
    error: ["help is-danger"]
  }
};
