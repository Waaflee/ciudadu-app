export default {
  path: "/login",
  component: () => import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  meta: {
    guest: true
  },
  children: [
    {
      path: "",
      props: { action: "login" },
      component: () =>
        import(/* webpackChunkName: "login" */ "../views/Login/Select.vue")
    },
    {
      path: "user",
      component: () =>
        import(
          /* webpackChunkName: "login" */ "../views/Login/LoginRouter.vue"
        ),
      props: { rol: "user" },
      children: [
        {
          path: "",
          props: { rol: "user" },
          component: () =>
            import(
              /* webpackChunkName: "login" */ "../views/Login/LoginForm.vue"
            )
        }
      ]
    },
    {
      path: "business",
      component: () =>
        import(
          /* webpackChunkName: "login" */ "../views/Login/LoginRouter.vue"
        ),
      props: { rol: "business" },
      children: [
        {
          path: "",
          props: { rol: "business" },
          component: () =>
            import(
              /* webpackChunkName: "login" */ "../views/Login/LoginForm.vue"
            )
        }
      ]
    },
    {
      path: "google",
      component: () =>
        import(/* webpackChunkName: "login" */ "../views/Login/Google.vue")
    },
    {
      path: "*",
      redirect: "/login"
    }
  ]
};
