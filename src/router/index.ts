import Vue from "vue";
import VueRouter, { Route, RouteConfig } from "vue-router";
import store from "@/store";
import login from "./login";
import signup from "./signup";
import user from "./user";
import business from "./business";
import admin from "./admin";
import { Users } from "@/store/services/users";
import client from "@/feathers-client";
Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    beforeEnter: async (to, from, next) => {
      if (store.getters["auth/isAuthenticated"]) {
        const user: Users = store.getters["auth/user"];
        if (!["admin", "business-admin", "user-admin"].includes(user.rol)) {
          if (user.isVerified) {
            if (user.isConfirmed) {
              const destination =
                store.getters["auth/user"].rol === "user"
                  ? "user"
                  : store.getters["auth/user"].rol === "business"
                  ? "business"
                  : "admin";
              next("/" + destination);
              console.log("is not finish user signup");
            } else {
              const userData = await client.service("users").get(user._id);
              if (userData.rol === "user" && !userData.university) {
                console.log("is not finish user signup");
                next("/signup/user/2");
              } else if (userData.rol === "business" && !userData.phone) {
                console.log("is not finish business signup");
                next("/signup/business/2");
              } else {
                console.log("is not Confirmed");
                next("/signup/confirm");
              }
            }
          } else {
            console.log("is not Verified");
            next("/signup/verify");
          }
        } else {
          console.log("is Admin");

          next("/admin");
        }
      } else {
        next("login");
      }
    }
  },
  login,
  signup,
  user,
  business,
  admin,
  {
    path: "*",
    redirect: "/"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const isAuthenticated: boolean = store.getters["auth/isAuthenticated"];
  // const user = store.getters["auth/user"];
  // if (
  //   !to.matched.some(record => record.meta.allowUnverified) &&
  //   isAuthenticated &&
  //   !user.isVerified
  // )
  //   next({ path: "/signup/verify" });
  // else if (
  //   to.matched.some(record => record.meta.allowUnconfirmed) &&
  //   isAuthenticated &&
  //   user.isVerified &&
  //   !user.isConfirmed
  // )
  //   next({ path: "/signup/confirm" });
  // else
  // console.log("AUTHENTICATED");
  // console.log(isAuthenticated);
  // console.log(!to.matched.some(record => record.meta.guest));
  // console.log(
  //   !to.matched.some(record => record.meta.guest) && !isAuthenticated
  // );
  if (!to.matched.some(record => record.meta.guest) && !isAuthenticated) {
    console.log("NAVIGATIOON GUARD");
    next({ path: "/login" });
  } else {
    next();
  }
  // next();
});

export default router;
