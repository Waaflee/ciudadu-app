import store from "@/store";
import { NavigationGuardNext, Route } from "vue-router";
export default {
  path: "/admin",
  component: () =>
    import(/* webpackChunkName: "admin" */ "../views/admin/Index.vue"),
  beforeEnter: (to: Route, from: Route, next: NavigationGuardNext) => {
    const user = store.getters["auth/user"];
    if (["admin", "business-admin", "user-admin"].includes(user.rol)) {
      next();
    } else {
      next("/");
    }
  },
  children: [
    {
      path: "",
      beforeEnter: (to: Route, from: Route, next: NavigationGuardNext) => {
        const admin = store.getters["auth/user"];
        const destination = admin.rol === "user-admin" ? "users" : "benefits";
        next("/admin/" + destination);
      }
      // component: () =>
      //   import(/* webpackChunkName: "admin" */ "../views/admin/Home.vue")
    },
    {
      path: "notifications",
      component: () =>
        import(
          /* webpackChunkName: "admin" */ "../views/admin/Notifications.vue"
        )
    },
    {
      path: "benefits",
      component: () =>
        import(/* webpackChunkName: "admin" */ "../views/admin/Benefits.vue")
    },
    {
      path: "benefits/add/:id",
      component: () =>
        import(/* webpackChunkName: "admin" */ "../views/admin/BenefitAdd.vue")
    },
    {
      path: "benefits/edit/:id",
      component: () =>
        import(/* webpackChunkName: "admin" */ "../views/admin/BenefitEdit.vue")
    },
    {
      path: "benefits/success",
      component: () =>
        import(
          /* webpackChunkName: "admin" */ "../views/admin/BenefitSuccess.vue"
        )
    },
    {
      path: "statistics",
      component: () =>
        import(/* webpackChunkName: "admin" */ "../views/admin/Statistics.vue")
    },
    {
      path: "users",
      component: () =>
        import(/* webpackChunkName: "admin" */ "../views/admin/Users.vue")
    }
    // {
    //   path: "contact",
    //   component: () =>
    //     import(/* webpackChunkName: "admin" */ "../views/Contact.vue")
    // }
  ]
};
