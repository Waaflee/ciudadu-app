import store from "@/store";
import { NavigationGuardNext, Route } from "vue-router";

export default {
  path: "/business",
  component: () =>
    import(/* webpackChunkName: "business" */ "../views/business/Index.vue"),
  beforeEnter: (to: Route, from: Route, next: NavigationGuardNext) => {
    const user = store.getters["auth/user"];
    if (user.rol === "business") {
      next();
    } else {
      next("/");
    }
  },
  children: [
    {
      path: "",
      component: () =>
        import(/* webpackChunkName: "business" */ "../views/business/Home.vue")
    },
    {
      path: "codes",
      component: () =>
        import(/* webpackChunkName: "business" */ "../views/business/Codes.vue")
    },
    {
      path: "benefits",
      component: () =>
        import(
          /* webpackChunkName: "business" */ "../views/business/Benefits.vue"
        )
    },
    {
      path: "benefits/edit/:id",
      component: () =>
        import(
          /* webpackChunkName: "business" */ "../views/business/BenefitEdit.vue"
        )
    },
    {
      path: "benefits/create",
      component: () =>
        import(
          /* webpackChunkName: "business" */ "../views/business/BenefitEdit.vue"
        )
    },
    {
      path: "benefits/success",
      component: () =>
        import(
          /* webpackChunkName: "business" */ "../views/business/BenefitSuccess.vue"
        )
    },
    {
      path: "statistics",
      component: () =>
        import(
          /* webpackChunkName: "business" */ "../views/business/Statistics.vue"
        )
    },
    {
      path: "profile",
      component: () =>
        import(
          /* webpackChunkName: "business" */ "../views/business/Profile.vue"
        )
    },
    {
      path: "contact",
      component: () =>
        import(/* webpackChunkName: "business" */ "../views/Contact.vue")
    }
  ]
};
