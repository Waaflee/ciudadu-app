import store from "@/store";
import { NavigationGuardNext, Route } from "vue-router";

export default {
  path: "/user",
  component: () =>
    import(/* webpackChunkName: "user" */ "../views/user/Index.vue"),
  beforeEnter: (to: Route, from: Route, next: NavigationGuardNext) => {
    const user = store.getters["auth/user"];
    if (user.rol === "user") {
      next();
    } else {
      next("/");
    }
  },
  children: [
    {
      path: "",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Home.vue")
    },
    {
      path: "codes",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Codes.vue")
    },
    {
      path: "benefit/:id",
      name: "benefitID",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Benefit.vue")
    },
    {
      path: "business/:id",
      name: "businessID",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Business.vue")
    },
    {
      path: "benefits",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Benefits.vue")
    },
    {
      path: "benefits/filter",
      name: "benefitsFilter",
      component: () =>
        import(
          /* webpackChunkName: "user" */ "../views/user/BenefitsFiltered.vue"
        )
    },

    {
      path: "card",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Card.vue")
    },
    {
      path: "profile",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/user/Profile.vue")
    },
    {
      path: "contact",
      component: () =>
        import(/* webpackChunkName: "user" */ "../views/Contact.vue")
    }
  ]
};
