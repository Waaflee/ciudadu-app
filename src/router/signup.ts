const user = {
  path: "user",
  component: () =>
    import(/* webpackChunkName: "signup" */ "../views/Login/LoginRouter.vue"),
  props: { rol: "user" },
  meta: {
    allowUnconfirmed: true,
    allowUnverified: true
  },
  children: [
    {
      path: "",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/UserFirst.vue"
        ),
      meta: {
        guest: true,
        allowUnconfirmed: true,
        allowUnverified: true
      }
    },
    {
      path: "2",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/UserSecond.vue"
        ),
      meta: { allowUnconfirmed: true }
    },
    {
      path: "3",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/UserThird.vue"
        ),
      meta: { allowUnconfirmed: true }
    }
  ]
};

const business = {
  path: "business",
  component: () =>
    import(/* webpackChunkName: "signup" */ "../views/Login/LoginRouter.vue"),
  props: { rol: "business" },
  children: [
    {
      path: "",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/BusinessFirst.vue"
        ),
      meta: {
        guest: true,
        allowUnconfirmed: true,
        allowUnverified: true
      }
    },
    {
      path: "2",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/BusinessSecond.vue"
        ),
      meta: {
        allowUnconfirmed: true
      }
    }
  ]
};

export default {
  path: "/signup",
  component: () =>
    import(/* webpackChunkName: "signup" */ "../views/Login.vue"),
  meta: {
    allowUnconfirmed: true,
    allowUnverified: true,
    guest: true
  },

  children: [
    {
      path: "",
      props: { action: "signup" },
      component: () =>
        import(/* webpackChunkName: "signup" */ "../views/Login/Select.vue"),
      meta: {
        guest: true,
        allowUnconfirmed: true,
        allowUnverified: true
      }
    },
    {
      path: "verify",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/NotVerified.vue"
        ),
      meta: {
        allowUnconfirmed: true,
        allowUnverified: true
      }
    },
    {
      path: "confirm",
      component: () =>
        import(
          /* webpackChunkName: "signup" */ "../views/SignUp/NotConfirmed.vue"
        ),
      meta: {
        allowUnconfirmed: true
      }
    },
    user,
    business
  ]
};
