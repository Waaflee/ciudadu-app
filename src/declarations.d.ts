declare module "@braid/vue-formulate" {
  import VueFormulate from "@braid/vue-formulate";
  export default VueFormulate;
}
declare module "vue-carousel" {
  import VueCarousel from "vue-carousel";
  export default VueCarousel;
}
declare module "vue2-circle-progress" {
  import VueCircle from "vue2-circle-progress";
  export default VueCircle;
}
declare module "easy-circular-progress" {
  import Progress from "easy-circular-progress";
  export default Progress;
}
declare module "vue-qr" {
  import VueQr from "vue-qr";
  export default VueQr;
}
declare module "dom-to-image" {
  import domtoimage from "dom-to-image";
  export default domtoimage;
}
declare module "v-chartist" {
  import VueChartist from "v-chartist";
  export default VueChartist;
}
declare module "vue-qrcode-reader" {
  import {
    QrcodeStream,
    QrcodeDropZone,
    QrcodeCapture
  } from "vue-qrcode-reader";
  export const { QrcodeStream, QrcodeDropZone, QrcodeCapture };
}
