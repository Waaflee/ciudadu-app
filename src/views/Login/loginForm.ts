export const loginForm = (rol: string) => {
  return [
    {
      component: "h1",
      children: "Ingresar",
      class: ["title", "is-size-3", "has-text-weight-bold"]
    },
    {
      component: "p",
      children: "Ingresá a nuestra plataforma con tu email registrado.",
      class: ["mb-5"]
    },
    {
      label: "Email*",
      name: "email",
      placeholder: "Ingrese su correo electrónico",
      class: ["has-text-grey"],
      validation: "bail|required|email",
      "validation-messages": {
        email: "Debes ingresar una dirección de correo válida.",
        required: "Debes ingresar una dirección de correo válida."
      }
    },
    {
      label: "Contraseña*",
      name: "password",
      type: "password",
      placeholder: "Ingrese su contraseña",
      validation: "required",
      "validation-messages": {
        required: "Debes ingresar una contraseña."
      }
    },
    {
      type: "submit",
      label: "Ingresar",
      "@click": "click-submit"
    },
    {
      component: "p",
      children: [
        {
          component: "span",
          children: "no tienes una cuenta? "
        },
        {
          component: "router-link",
          to: "/signup/" + rol,
          children: "Registrarse",
          class: ["has-text-weight-bold", "has-text-info"]
        }
      ]
    }
  ];
};
