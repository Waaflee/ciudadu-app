export default [
  {
    field: "name",
    label: "Nombre",
    searchable: true
  },
  {
    field: "rol",
    label: "Tipo de Usuario",
    searchable: true
  },
  {
    field: "dni",
    label: "DNI",
    numeric: true,
    searchable: true
  },
  {
    field: "isConfirmed",
    label: "Activado",
    searchable: true
  },
  //   {
  //     field: "createdAt",
  //     label: "Fecha de Creación"
  //   }
  //   {
  //     field: "name",
  //     label: "Editar"
  //   },
  {
    field: "isConfirmed",
    label: "Activar/Desactivar"
  }
];
