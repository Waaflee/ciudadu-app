import { Ref, ref } from "@vue/composition-api";

export const hasError = ref({ hasError: false, message: "" });
export const data = (rol: string): Ref<any> => ref({ rol: rol });

export const handleError = (err: Error) => {
  hasError.value.hasError = true;
  hasError.value.message = err.message;
  console.log(err);
};
