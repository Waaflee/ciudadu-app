import { Benefits } from "@/store/services/benefits";
export default (data: Partial<Benefits>) => {
  return [
    {
      label: "Nombre del descuento / beneficio*",
      type: "text",
      placeholder: data.name ?? "",
      name: "name",
      class: ["formulate-edit"]
    },
    {
      component: "div",
      class: ["columns", "is-mobile"],
      children: [
        {
          label: "Categoría*",
          type: "select",
          options: [
            "Instituciones",
            "Gastronomía",
            "Automotores",
            "Tiendas y Mercados",
            "Servicios Varios",
            "Salud",
            "Servicios de Emergencia",
            "Indumentaria y Accesorios",
            "Cuidado Personal",
            "Librería e Impresiones",
            "Transporte",
            "Cultura y Diversión",
            "Capacitación y Desarrollo",
            "Informática",
            "Turismo y Alojamiento",
            "Deportes",
            "Hogar"
          ],
          placeholder: data.category ?? "",
          name: "category",
          class: ["column"]
        }
        // {
        //   label: "Sub Categoría*",
        //   type: "select",
        //   options: ["jajaj", "jejej"],
        //   placeholder: data.subcategory ?? "",
        //   name: "subcategory",
        //   class: ["column"]
        // }
      ]
    },
    {
      component: "div",
      class: ["columns", "is-mobile"],
      children: [
        {
          label: "Tipo de descuento*",
          type: "select",
          options: ["Descuento", "Promoción"],
          placeholder: data.subcategory ?? "",
          name: "type",
          class: ["column"]
        },
        {
          label: "Porcentaje*",
          type: "text",
          placeholder: data.percentage ?? "",
          name: "percentage",
          class: ["column"]
        }
      ]
    },
    {
      component: "div",
      class: ["columns"],
      children: [
        {
          label: "Tipo de pago*",
          type: "select",
          options: ["Efectivo", "Crédito", "Débito"],
          placeholder: data.acceptedPayments ?? "",
          name: "acceptedPayments",
          class: ["column"]
        },

        {
          component: "div",
          class: ["formulate-input", "field", "column"],
          children: [
            {
              component: "p",
              children: "Días de Descuento*",
              class: ["label", "has-text-grey"]
            },
            {
              component: "b-taginput",
              name: "days",
              class: ["taginput-field"],
              style: ["height: 100%;"],
              "@input": "taginput",
              ellipsis: "",
              icon: "label",
              autocomplete: true,
              rounded: true,
              maxtags: "5",
              maxlength: "9",
              data: [
                "lunes",
                "martes",
                "miércoles",
                "jueves",
                "viernes",
                "sábado",
                "domingo"
              ],
              "has-counter": false,
              placeholder: "lunes",
              "aria-close-label": "Borrar este día"
            }
          ]
        }
      ]
    },
    {
      component: "div",
      class: ["columns", "is-mobile"],
      children: [
        {
          label: "Guardar",
          type: "submit",
          class: ["column", "my-0"],
          "@click": "click-submit"
        },
        {
          label: "Borrar",
          type: "button",
          class: ["column", "my-0"],
          "@click": "click-delete"
        }
      ]
    }
  ];
};
