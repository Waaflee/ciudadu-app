(SELECT 
    'businessName', 
    'businessReview',
    'phone',
    'address',
    'email',
    'businessLink')
UNION
(SELECT 
    nombre AS businessName, 
    resenia AS businessReview,
    telefono AS phone,
    direccion AS address,
    email,
    web AS businessLink
FROM
    integrantes
WHERE
    deshabilitado = 0 
INTO OUTFILE '/tmp/business.csv' 
FIELDS ENCLOSED BY '' 
TERMINATED BY ',' 
ESCAPED BY '' 
LINES TERMINATED BY '\n');


-- export interface Users {
--   _id?: Id;
--   email: string;
--   password: string;
--   rol: "user" | "business" | "business-admin" | "user-admin" | "admin";
--   profilePicture?: string;
--   businessPicture?: string;
--   businessCategory?: string;
--   businessName?: string;
--   businessReview?: string;
--   businessLink?: string;
--   name?: string;
--   lastName?: string;
--   birthday?: string;
--   dni?: number;
--   university?: string;
--   facultyAndCareer?: string;
--   certificate?: string;
--   registration?: number;
--   sex?: string;
--   phone?: string;
--   address?: string;
--   isConfirmed: boolean;
--   googleId?: string;
--   isVerified?: boolean;
--   verifyToken?: string;
--   verifyExpires?: Date;
--   verifyChanges?: unknown;
--   resetToken?: string;
--   resetExpires?: Date;
-- }